<?php

namespace AppBundle\Controller;

use AppBundle\Form\FormType;
use AppBundle\Model\Form;
use AppBundle\Model\FormItem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    /**
     * @Method({"GET", "POST"})
     * @Route("/login", name="login")
     * @param Request $request
     */
    public function loginAction()
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(':default:login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/{formId}", name="form_application")
     * @param Request $request
     * @param $formId
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function indexAction(Request $request, $formId)
    {
        // TODO: construir form en servicio
        // TODO: proveedor de forms desde base de datos
        $formsConfig = $this->getParameter('forms');

        if (!isset($formsConfig[$formId])) {
            throw new NotFoundHttpException('Formulario no encontrado');
        }

        $config = $formsConfig[$formId];

        $form = Form::create(
            $config['title'],
            array_map([FormItem::class, 'create'], $config['items'])
        );

        $formApplication = $this->createForm(FormType::class, $form);
        $formApplication->handleRequest($request);

        if ($formApplication->isSubmitted() && $formApplication->isValid()) {
            // persistencia
            // return $this->redirectToRoute('homepage');
        }
        
        return $this->render(':default:index.html.twig', [
            'form_config' => $config,
            'form' => $formApplication->createView()
        ]);
    }
    
    /**
     * @Route("/", name="homepage")
     */
    public function listAction()
    {
        return $this->render(':default:list.html.twig', [
            'forms_list' => $this->getParameter('forms'),
        ]);
    }


}
