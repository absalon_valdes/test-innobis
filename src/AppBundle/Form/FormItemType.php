<?php

namespace AppBundle\Form;

use AppBundle\Model\FormItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var FormItem $data */
            $data = $event->getData();
            
            /** @var FormInterface $form */
            $form = $event->getForm();
            
            $form->add('value', ChoiceType::class, [
                'label' => $data->getText(),
                'expanded' => true,
                'multiple' => false,
                'choices' => ['cumple', 'no_cumple', 'no_aplica'],
                'choice_label' => function ($v) {
                    return $v;
                },
            ]);
        });
    }
    
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', FormItem::class);
    }
    
}