<?php

namespace AppBundle\Form;

use AppBundle\Model\Form;
use AppBundle\Model\FormItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var FormInterface $form */
            $form = $event->getForm();
            
            /** @var Form $formConfig */
            $formConfig = $event->getData();
    
            /** @var FormItem $item */
            foreach ($formConfig->getItems() as $i => $item) {
                $form->add('items', CollectionType::class, [
                    'label' => false,
                    'entry_type' => FormItemType::class,
                    'entry_options' => [
                        'label' => false,
                    ],
                ]);
            }
        });
    }
    
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Form::class);
    }
}