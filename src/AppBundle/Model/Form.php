<?php

namespace AppBundle\Model;

/**
 * Class Form
 * @package AppBundle\Model
 */
class Form
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var array
     */
    private $items = [];

    /**
     * Form constructor.
     * @param $title
     * @param array $items
     */
    public function __construct($title, array $items)
    {
        $this->title = $title;
        $this->items = $items;
    }

    /**
     * @param $title
     * @param array $items
     * @return Form
     */
    public static function create($title, array $items)
    {
        return new self($title, $items);
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param mixed $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @param FormItem $item
     */
    public function addItem(FormItem $item)
    {
        $this->items[] = $item;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}

