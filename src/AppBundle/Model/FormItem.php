<?php

namespace AppBundle\Model;

/**
 * Class FormItem
 * @package AppBundle\Model
 */
class FormItem
{
    /**
     * @var string
     */
    protected $value;

    /**
     * @var string
     */
    protected $text;

    /**
     * @param $text
     * @return FormItem
     */
    public static function create($text)
    {
        return new self($text);
    }

    /**
     * FormItem constructor.
     * @param $text
     */
    public function __construct($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }
}

